import os
from flask import Flask, flash, request, redirect, url_for, render_template
import tf_run_wrapper
import json
import cv2

UPLOAD_FOLDER = './static'
ALLOWED_EXTENSIONS = set(['mp4', 'avi'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'

cap = cv2.VideoCapture(0)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET','POST'])
def main():
    return render_template('form.html', data=json.dumps({"sents":[]}))

@app.route('/upload_file', methods=['GET', 'POST'])
def upload_file():
    event_sents_list = []; video_list = []
    error_msg = None

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

        event_sents_list, video_list = tf_run_wrapper.demo_with_file(os.path.join(UPLOAD_FOLDER, file.filename))

        try:
            event_sents_list, video_list = tf_run_wrapper.demo_with_file(os.path.join(UPLOAD_FOLDER, file.filename))
        except ValueError:
            error_msg = 'Video length is too short, try with longer video!'

        result_obj = {'sents': event_sents_list,
                      'video_list': video_list,
                      'error_msg': error_msg}

        return render_template('form.html', data=json.dumps(result_obj))

    return render_template('form.html', data=json.dumps({"sents":[]}))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=40000)