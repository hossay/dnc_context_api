import tensorflow as tf
import os,sys,re
import numpy as np
import time

sys.path.insert(0, os.path.abspath('./module'))

from config import FLAGS
import data_utils
import model_zoo
import dnc_c3d_model
from extract_c3d_feats import build_graph as build_c3d_graph
import datetime
import json
import cv2
import glob, os


# read dataset (only validation set)
dataset = json.load(open('./module/Dense_VTT/annotation/val_1.json'))

word2ix = json.load(open('./module/word2ix.json'))
ix2word = { v:k for k,v in word2ix.items() }

# model configureations
batch_size = FLAGS.batch_size
words_count = FLAGS.words_count
word_size = FLAGS.word_size
read_heads = FLAGS.read_heads

os.environ["CUDA_VISIBLE_DEVICES"]=str(FLAGS.gpu)

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

g_c3d = tf.Graph()
with g_c3d.as_default():
    ph_c3d = dict(video_clip=tf.placeholder(tf.float32,
                                            [1, None, FLAGS.IMG_SIZE, FLAGS.IMG_SIZE, 3],
                                            name='tf_video_clip'))

    net = None
    if FLAGS.pretrained=='sports1m':
        # build C3D network
        net = model_zoo.C3DNet(
            pretrained_model_path='../pretrained/C3D/conv3d_deepnetA_sport1m_iter_1900000_TF.model', trainable=False)
    elif FLAGS.pretrained=='kinetic':
        net = model_zoo.I3DNet(inps=ph_c3d['video_clip'],
                               pretrained_model_path='../pretrained/i3d-tensorflow/kinetics-i3d/data/kinetics_i3d/model',
                               final_end_point='FeatureExtraction', trainable=False, scope='v/SenseTime_I3D')

    m_c3d = build_c3d_graph(ph_c3d, net, FLAGS.DELTA)

    sess_c3d = tf.Session(graph=g_c3d, config=config)
    sess_c3d.run(tf.global_variables_initializer())

    if FLAGS.pretrained=='kinetic':
        # init all variables with pre-trained ckpt
        sess_c3d.run(net.assign_ops)

g_cap = tf.Graph()
with g_cap.as_default():
    # placeholders only for decoder
    ph_cap = dict(n_events=tf.placeholder(tf.int32, name='n_events'),
              multi_input_data=tf.placeholder(tf.float32, shape=[None,None,None,FLAGS.input_size], name='multi_input_data'),
              multi_dec_in=tf.placeholder(tf.int32, shape=[None,None, None], name='multi_dec_in'),
              multi_dec_target=tf.placeholder(tf.int32, shape=[None,None, None], name='multi_dec_target'),
              )

    m_cap = dnc_c3d_model.build_graph(ph_cap, is_train=False)

    sess_cap = tf.Session(graph=g_cap, config=config)
    sess_cap.run(tf.global_variables_initializer())

    # restore to continue training...
    if os.path.exists(FLAGS.checkpoint_dir):
        print("Restoring Checkpoint %s ... " % (FLAGS.checkpoint_dir))
        tf.train.Saver(tf.trainable_variables()).restore(sess_cap, os.path.join(FLAGS.checkpoint_dir, 'model.ckpt'))
        print("Done!\n")

shape_list = [[batch_size, words_count, word_size],
              [batch_size, words_count, ],
              [batch_size, words_count, ],
              [batch_size, words_count, words_count],
              [batch_size, words_count, ],
              [batch_size, words_count, read_heads],
              [batch_size, word_size, read_heads]]

g_cap_online = tf.Graph()
with g_cap_online.as_default():
    ph_cap_online = dict(n_events=tf.placeholder(tf.int32, name='n_events'),
                         input_data=tf.placeholder(tf.float32, shape=[None,None,FLAGS.input_size], name='input_data'),
                         memory_state=tuple([ tf.placeholder(tf.float32, shape=shape, name='memory_val_{}'.format(i)) for i,shape in enumerate(shape_list) ])
                         )

    m_cap_online = dnc_c3d_model.build_online_graph(ph_cap_online)

    sess_cap_online = tf.Session(graph=g_cap_online, config=config)
    sess_cap_online.run(tf.global_variables_initializer())

    # restore to continue training...
    if os.path.exists(FLAGS.checkpoint_dir):
        print("Restoring Checkpoint %s ... " % (FLAGS.checkpoint_dir))
        tf.train.Saver(tf.trainable_variables()).restore(sess_cap_online, os.path.join(FLAGS.checkpoint_dir, 'model.ckpt'))
        print("Done!\n")



def extract_feats(cap, frame_cnt=16, sample_rate=10):
    video_clip = []

    i = 0
    t = 0

    while True:
        ret, frame = cap.read()
        if not ret or len(video_clip)==frame_cnt:
            break
        cv2.imshow('cam', frame)
        cv2.waitKey(1)

        if i % sample_rate == 0:
            if FLAGS.pretrained == 'sports1m':
                crop_mean = np.load('./module/sports1m-mean.npy')[0].transpose(1, 2, 3, 0)  # (16, 128, 171, 3)
                frame = data_utils.preprocess(frame, crop_mean.mean(axis=0))
            elif FLAGS.pretrained == 'kinetic':
                frame = cv2.resize(frame, (FLAGS.IMG_SIZE, FLAGS.IMG_SIZE))
                frame = frame.astype(np.float32)

            video_clip.append(frame)
            t += 1

        i += 1

    return sess_c3d.run(m_c3d['feats_sequence'], feed_dict={ph_c3d['video_clip']: [video_clip]})

def split_video(video_file, timestamps):
    file_list = []
    for ix, (start, end) in enumerate(timestamps):
        if start > end:
            start,end = end,start

        start = str(datetime.timedelta(seconds=start))
        end = str(datetime.timedelta(seconds=end))

        out_filename = '{}_{}'.format(os.path.splitext(video_file)[0],ix)+'.mp4'
        cmd = 'ffmpeg -y -ss {} -i {} -to {} -c copy {}'.format(start, video_file, end, out_filename)
        os.system(cmd)

        file_list.append(out_filename)

    return file_list


def demo_with_file(video_file):
    # WAY #2 : Load pre-extracted features from disk
    if FLAGS.pretrained=='sports1m':
        FEATS_HOME = '/mnt/hdd1/Dataset/Dense_VTT/c3d_feats/fc7/val_1'
    elif FLAGS.pretrained=='kinetic':
        FEATS_HOME = '/mnt/hdd1/Dataset/Dense_VTT/c3d_feats_kinetic/val_1'
    video_filename = os.path.splitext(os.path.basename(video_file))[0]

    feats_files = sorted(glob.glob(os.path.join(FEATS_HOME, video_filename, '*')))


    feats_sequence = map(lambda x: np.load(x).squeeze(axis=0), feats_files)

    # feats padding
    max_len = max(map(lambda x: x.shape[0], feats_sequence))
    n_events = len(feats_sequence)

    padded_feats = np.zeros([n_events, 1, max_len, FLAGS.input_size], dtype=np.float32)

    for i in range(n_events):
        padded_feats[i,0,:feats_sequence[i].shape[0]] = feats_sequence[i]

    multi_dec_logits = sess_cap.run(m_cap['multi_dec_logits'], feed_dict={ph_cap['n_events']: n_events,
                                                                           ph_cap['multi_input_data']: padded_feats})

    event_sents_list = []

    for k in range(n_events):
        hypo = ' '.join([ix2word[ix] for ix in multi_dec_logits[k][0].argmax(axis=-1)])
        hypo = hypo.encode('utf-8')
        hypo = re.sub(r'<EOS>.*', '', hypo).strip().capitalize()
        event_sents_list.append(hypo)

    # TODO. sould be modified to list of subscene video path
    timestamps = dataset[video_filename]['timestamps']
    video_list = split_video(video_file, timestamps)
    # video_list = [video_file] * n_events

    print(video_list)

    return event_sents_list, video_list


def demo_with_cam(cap):
    # WAY #1 : REAL feature extraction
    t0 = time.time()
    feats_sequence = extract_feats(cap=cap)
    print('[feature extraction]: {0:.3f}'.format(time.time()-t0))

    t1 = time.time()
    memory_state = m_cap_online['ncomputer'].memory.memory_state

    new_memory_state, dec_logits = sess_cap_online.run([m_cap_online['new_memory_state'],
                                                        m_cap_online['dec_logits']],
                                                       feed_dict={ph_cap_online['input_data']: feats_sequence,
                                                                  ph_cap_online['memory_state']: memory_state})

    m_cap_online['ncomputer'].memory.update_memory_state(new_memory_state)

    print('[inference]: {0:.3f}'.format(time.time()-t1))


    hypo = ' '.join([ix2word[ix] for ix in dec_logits[0].argmax(axis=-1)])
    hypo = hypo.encode('utf-8')
    hypo = re.sub(r'<EOS>.*', '', hypo).strip().capitalize()

    print(hypo)

    return hypo

cap = cv2.VideoCapture(0)
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 224)
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 224)
cap.set(cv2.CAP_PROP_AUTOFOCUS, False)

while True:
    demo_with_cam(cap)