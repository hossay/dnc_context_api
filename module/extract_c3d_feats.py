import tensorflow as tf
import numpy as np
import sys, os

import time, datetime
import model_zoo
from data_utils import FrameBatcher
from config import FLAGS

# static vars
BATCH_SIZE = 1
SAMPLE_RATE = 10
IMG_SIZE = FLAGS.IMG_SIZE

os.environ["CUDA_VISIBLE_DEVICES"]=str(FLAGS.gpu)

def llprint(message):
    sys.stdout.write(message)
    sys.stdout.flush()

def get_feats_sequence(net, tf_video_clip, DELTA):
    '''
    Extract c3d features from tf_video_clip
    :param net: c3dnet instance
    :param tf_video_clip: placeholder for input video clip at specific event index
                          shape = (batch_size, n_frames, height, width, 3)
    :return: sequence of extracted feature from given video clip
    '''
    def body(i, ta):
        sliced_inputs = tf_video_clip[:,i*DELTA:(i+1)*DELTA]
        #paddings = ((0, 0), (0, (i + 1) * DELTA - tf.shape(sliced_inputs)[1]), (0, 0), (0, 0), (0, 0))
        #padded_inputs = tf.pad(sliced_inputs, paddings, 'CONSTANT')
        c3d_feats = net(inputs=sliced_inputs)
        ta = ta.write(i,c3d_feats)
        return i+1, ta

    i = tf.constant(0)
    out_length = tf.maximum(tf.shape(tf_video_clip)[1] / DELTA, 1)

    ta = tf.TensorArray(dtype=tf.float32, size=out_length, name='ta')
    _, feats_sequence = tf.while_loop(cond=lambda i,*_ : i<out_length,
                                      body=body,
                                      loop_vars=(i,ta))

    return tf.transpose(feats_sequence.stack(), (1,0,2))



def build_graph(ph, net, DELTA):
    # placeholders
    video_clip = ph['video_clip']

    # c3d feats_sequence
    feats_sequence = get_feats_sequence(net, video_clip, DELTA)

    return dict(feats_sequence=feats_sequence)


def step(sess, g, ph, batcher, dir):
    feed_data = batcher.prepare_feed_data(session=sess,
                                          feats_sequence=g['feats_sequence'],
                                          tf_video_clip=ph['video_clip'])

    _input_data_list = feed_data

    vid, data_list = _input_data_list[0][0], _input_data_list[1:]
    _dir = os.path.join(dir, vid)
    if not os.path.exists(_dir):
        os.makedirs(_dir)

    for i in range(len(data_list)):
        file_path = os.path.join(_dir, 'event-{}.npy'.format(i))
        np.save(file_path, data_list[i])

def save_feats(g, ph, sess, batcher, dir):
    N = len(batcher.data)

    spent_list = []
    for i in range(N):
        t0 = time.time()
        step(sess,g,ph,batcher,dir)
        spent = int(time.time()-t0)
        spent_list.append(spent)
        avg = int(sum(spent_list)/len(spent_list))
        avg_time = str(datetime.timedelta(seconds=avg))
        remaining_time = str(datetime.timedelta(seconds=avg*(N-(i+1))))
        print '[ Thread : {} ], {}/{}, time per each sample : {} remaining : {}'.format(batcher.thread_index,i+1,N, avg_time, remaining_time)

def main():
    import collections
    import json
    import threading

    n_threads = 8

    data = dict(train=collections.OrderedDict(json.load(file('./Dense_VTT/annotation/train.json'))).items(),
                val_1=collections.OrderedDict(json.load(file('./Dense_VTT/annotation/val_1.json'))).items())

    batchers = []
    for i in range(n_threads):
        files_per_thread = len(data[FLAGS.type])/n_threads
        frag = data[FLAGS.type][i * files_per_thread:(i + 1) * files_per_thread]

        _batcher = FrameBatcher(data=frag, type=FLAGS.type, thread_index=i)
        batchers.append(_batcher)

    # build TF model
    ph = dict(video_clip=tf.placeholder(tf.float32,
                                        [BATCH_SIZE, None, IMG_SIZE, IMG_SIZE, 3],
                                        name='tf_video_clip'))

    net = None
    if FLAGS.pretrained=='sports1m':
        # build C3D network
        net = model_zoo.C3DNet(
            pretrained_model_path='../pretrained/C3D/conv3d_deepnetA_sport1m_iter_1900000_TF.model', trainable=False)
    elif FLAGS.pretrained=='kinetic':
        net = model_zoo.I3DNet(inps=ph['video_clip'],
                               pretrained_model_path='../pretrained/i3d-tensorflow/kinetics-i3d/data/kinetics_i3d/model',
                               final_end_point='FeatureExtraction', trainable=False, scope='v/SenseTime_I3D')

    g = build_graph(ph, net, FLAGS.DELTA)

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    llprint("Initializing Variables ... \n")
    sess.run(tf.global_variables_initializer())
    llprint("Done!\n")

    if FLAGS.pretrained=='kinetic':
        # init all variables with pre-trained ckpt
        sess.run(net.assign_ops)

    threads = []
    for batcher in batchers:
        _thread = threading.Thread(target=save_feats, args=(g, ph, sess, batcher, os.path.join(FLAGS.dir, FLAGS.type)))
        threads.append(_thread)

    for th in threads:
        th.daemon = True
        th.start()

    for th in threads:
        th.join()

if __name__ == '__main__':
    main()
