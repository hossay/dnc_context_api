import sys
import tensorflow as tf
import numpy as np
from config import FLAGS
import json
import re

word2ix = json.load(file('./module/word2ix.json'))

def llprint(message):
    sys.stdout.write(message)
    sys.stdout.flush()


def inv_dict(dictionary):
    return {v: k for k, v in dictionary.iteritems()}

def rearange(batch, max_events, pad_shape, dtype):
    # re-arange
    rearanged_batch = []
    for ix in range(max_events):
        _batch = []

        for e in batch:
            try:
                sample = e[ix]
            except:
                sample = np.zeros(pad_shape, dtype).tolist()
            _batch.append(sample)
        rearanged_batch.append(_batch)
    return rearanged_batch

def get_status(n_events, multi_dec_logits, multi_dec_target):
    event_sentences = dict(gold=[], hypo=[])

    for k in range(n_events):
        gold = ' '.join([inv_dict(word2ix)[ix] for ix in multi_dec_target[k][0]])
        hypo = ' '.join([inv_dict(word2ix)[ix] for ix in multi_dec_logits[k][0].argmax(axis=-1)])

        gold = re.sub(r'<EOS>.*','',gold).capitalize()
        hypo = re.sub(r'<EOS>.*','',hypo).capitalize()

        event_sentences['gold'].append(gold)
        event_sentences['hypo'].append(hypo)

    return event_sentences


def dynamic_embedding(sent, embedding, max_glove_ix):

    def dynamic_embedding_each_sent(time_stamp, tensor_array):
        cur_word_id = sent[0, time_stamp]
        cur_emb = tf.cond(cur_word_id < max_glove_ix,
                          lambda: tf.nn.embedding_lookup(embedding['glove'], [cur_word_id]),
                          lambda: tf.nn.embedding_lookup(embedding['random'], [cur_word_id-max_glove_ix]))
        tensor_array = tensor_array.write(time_stamp, cur_emb)

        return time_stamp + 1, tensor_array

    time_stamp = tf.constant(0, dtype=tf.int32)
    sent_length = tf.shape(sent)[1]
    embeding_inputs = tf.TensorArray(dtype=tf.float32, size=sent_length)

    # prev_sentence embedding layer
    with tf.variable_scope('sentence_embedding_layer'):
        _, embeding_inputs = tf.while_loop(cond=lambda time_stamp, *_: time_stamp < sent_length,
                                                 body=dynamic_embedding_each_sent,
                                                 loop_vars=(time_stamp, embeding_inputs))

        embeding_inputs = tf.transpose(embeding_inputs.stack(), (1, 0, 2))

    return embeding_inputs


def write_logs(fn, step, loss):
    fd = open(fn, 'a+')
    line = '{}\t{}\n'.format(step, loss)
    fd.write(line)
    fd.close()

def _linear(inputs, output_size, use_bias=True, scope='custom_linear'):
    '''
    Simple custom _linear wrapper func, which flatten all inputs and projection to output_size space
    :param inputs:
    :param output_size:
    :param use_bias:
    :param scope:
    :return:
    '''
    total_inp_size = 0
    flat_inputs = []
    for inp in inputs:
        shape = inp.get_shape().as_list()
        # flatten each input, regardless of rank
        flat_inp = tf.reshape(inp, (FLAGS.batch_size, -1))  # (batch, dim_flatten)
        flat_inputs.append(flat_inp)
        total_inp_size += reduce(lambda x, y: x * y, shape[1:])

    with tf.variable_scope(scope):
        w = tf.get_variable('w', shape=[total_inp_size, output_size])
        res = tf.matmul(tf.concat(flat_inputs, 1), w)
        if use_bias:
            b = tf.get_variable('b', shape=[output_size], initializer=tf.constant_initializer(0.1))
            res += b
        return res


def context_controller(x, state, _forget_bias=1.0, _activation=tf.nn.tanh):
    '''
    Control context, which applies LSTM style gating mechanism.
    :param x: external input
    :param state: previous memory states dict containing (c, h)
    :param _forget_bias: Biases of the forget gate are initialized by default to 1
    in order to reduce the scale of forgetting at the beginning of
    the training.
    :param _activation: tf.nn.tanh as default

    :return: updated memory state dict containing (c, h)
    '''
    # prev states
    prev_c = state['c']  # DNC memory matrix -> LTM
    prev_h = state['h']  # containing history

    dim = prev_h.get_shape().as_list()[-1]
    _matrix = _linear([x, prev_h], dim * 4, scope='context_linear')

    # i = input_gate, j = new_input, f = forget_gate, o = output_gate
    i, j, f, o = tf.split(_matrix, 4, axis=1)

    new_c = (tf.nn.sigmoid(f + _forget_bias) * prev_c + tf.nn.sigmoid(i) * _activation(j))
    new_h = _activation(new_c) * tf.nn.sigmoid(o)

    return dict(c=new_c, h=new_h)


def RNNCellWrapper(num_units, num_layers, keep_prob, attention=False, attention_mechanism=None):
    cells = []
    for _ in range(num_layers):
        cell = tf.nn.rnn_cell.GRUCell(num_units=num_units)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob)
        cells.append(cell)

    cell = tf.nn.rnn_cell.MultiRNNCell(cells)

    if attention:
        # decoder cell with attention
        cell = tf.contrib.seq2seq.AttentionWrapper(cell=cell, attention_mechanism=attention_mechanism, attention_layer_size=num_units)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=keep_prob)

    return cell