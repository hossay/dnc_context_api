import tensorflow as tf
import sys
import json

from dnc.dnc import DNC
from dnc.recurrent_controller import RecurrentController
import tf_decoder
from config import FLAGS

# dict word->ix
word2ix = json.load(file('./module/word2ix.json'))

# tf flags configurations
batch_size = FLAGS.batch_size
input_size = FLAGS.input_size
output_size = FLAGS.output_size
word_space_size = len(word2ix)
words_count = FLAGS.words_count
word_size = FLAGS.word_size
read_heads = FLAGS.read_heads
hidden_size = FLAGS.hidden_size         # decoder hidden size
embedding_size = FLAGS.embedding_size

def llprint(message):
    sys.stdout.write(message)
    sys.stdout.flush()

def build_graph(ph, is_train):
    llprint("Building Computational Graph for DNC ...\n")

    with tf.variable_scope('DNC', reuse=tf.AUTO_REUSE):
        ncomputer = DNC(controller_class=RecurrentController,
                        input_size=input_size,
                        output_size=output_size,
                        keep_prob=ph.get('keep_prob', 1.0),
                        initial_memory_state=None,
                        memory_words_num=words_count,
                        memory_word_size=word_size,
                        memory_read_heads=read_heads,
                        batch_size=batch_size)

    with tf.variable_scope('embedding_layer', reuse=tf.AUTO_REUSE):
        embedding = tf.get_variable(name='embedding', shape=[word_space_size, embedding_size],
                                    initializer=tf.random_uniform_initializer(minval=-0.1,maxval=0.1))

    # dec_cell
    dec_cell = tf.nn.rnn_cell.GRUCell(num_units=hidden_size)

    def loop_body(t, multi_dec_logits, memory_state):
        ncomputer.build_graph(input_data=tf.reverse(ph['multi_input_data'][t], axis=[1]), # reverse input data to reduce curse of padding
                              initial_memory_state=memory_state)
        outputs, memory_veiw = ncomputer.get_outputs()
        new_memory_state = memory_veiw['new_memory_state']

        initial_state = outputs[:, -1]

        if is_train:
            # build computation graph for decoder
            rnn_outputs, _ = tf.nn.static_rnn(cell=dec_cell,
                                              inputs=tf.unstack(
                                                  tf.nn.embedding_lookup(embedding, ph['multi_dec_in'][t]), num=FLAGS.dec_maxlen, axis=1
                                              ),
                                              sequence_length=tf.reduce_sum(tf.sign(ph['multi_dec_target'][t]), axis=1),
                                              initial_state=initial_state)
            rnn_outputs = tf.stack(rnn_outputs, axis=1, name='rnn_outputs')

            dec_logits = tf.layers.dense(rnn_outputs, word_space_size,
                                         use_bias=False,
                                         name='rnn/decoder_projection')
        else:
            dec_logits = tf_decoder.greedy_decoder(cell=dec_cell,
                                                   embedding=embedding,
                                                   initial_state=initial_state,
                                                   word_space_size=word_space_size,
                                                   dec_maxlen=FLAGS.dec_maxlen)
            # padded logits
            dec_logits = tf.pad(dec_logits,
                                [[0,0],[0,FLAGS.dec_maxlen-tf.shape(dec_logits)[1]],[0,0]])

        multi_dec_logits = multi_dec_logits.write(t, dec_logits)

        return t + 1, multi_dec_logits, new_memory_state

    memory_state = ncomputer.memory.init_memory(None)
    t = tf.constant(0)

    multi_dec_logits = tf.TensorArray(dtype=tf.float32, size=ph['n_events'])
    _, multi_dec_logits, _ = tf.while_loop(cond=lambda t,*_ : t<ph['n_events'],
                                           body=loop_body,
                                           loop_vars=(t, multi_dec_logits,memory_state))


    return dict(ncomputer=ncomputer,
                embedding=embedding,
                multi_dec_logits=multi_dec_logits.stack())


def build_online_graph(ph):
    llprint("Building Computational Graph for DNC ...\n")

    with tf.variable_scope('DNC', reuse=tf.AUTO_REUSE):
        ncomputer = DNC(controller_class=RecurrentController,
                        input_size=input_size,
                        output_size=output_size,
                        keep_prob=ph.get('keep_prob', 1.0),
                        initial_memory_state=None,
                        memory_words_num=words_count,
                        memory_word_size=word_size,
                        memory_read_heads=read_heads,
                        batch_size=batch_size)

    with tf.variable_scope('embedding_layer', reuse=tf.AUTO_REUSE):
        embedding = tf.get_variable(name='embedding', shape=[word_space_size, embedding_size],
                                    initializer=tf.random_uniform_initializer(minval=-0.1,maxval=0.1))

    # dec_cell
    dec_cell = tf.nn.rnn_cell.GRUCell(num_units=hidden_size)

    ncomputer.build_graph(input_data=ph['input_data'],
                          initial_memory_state=ph['memory_state'])
    outputs, memory_veiw = ncomputer.get_outputs()

    new_memory_state = memory_veiw['new_memory_state']

    initial_state = outputs[:, -1]

    dec_logits = tf_decoder.greedy_decoder(cell=dec_cell,
                                           embedding=embedding,
                                           initial_state=initial_state,
                                           word_space_size=word_space_size,
                                           dec_maxlen=FLAGS.dec_maxlen)
    # padded logits
    dec_logits = tf.pad(dec_logits,
                        [[0,0],[0,FLAGS.dec_maxlen-tf.shape(dec_logits)[1]],[0,0]])

    return dict(ncomputer=ncomputer, new_memory_state=new_memory_state,
                embedding=embedding,
                dec_logits=dec_logits)