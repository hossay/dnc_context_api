import tensorflow as tf
# for multi rnn
tf.app.flags.DEFINE_integer("num_layers", 2, "num_layers")

# model config
tf.app.flags.DEFINE_integer("batch_size", 1, "batch size")
tf.app.flags.DEFINE_integer("input_size", 4096, "each input vector size")
tf.app.flags.DEFINE_integer("output_size", 1024, "each output vector size")
tf.app.flags.DEFINE_integer("words_count", 256, "words_count")
tf.app.flags.DEFINE_integer("word_size", 64, "word_size")
tf.app.flags.DEFINE_integer("read_heads", 4, "read_heads")
tf.app.flags.DEFINE_float("learning_rate", 5e-5, "learning rate")
tf.app.flags.DEFINE_integer("hidden_size", 1024, "hidden_size")
tf.app.flags.DEFINE_integer("embedding_size", 300, "embedding_size")
tf.app.flags.DEFINE_integer("dec_maxlen", 100, "dec_maxlen")

tf.app.flags.DEFINE_integer("height", 112, "height of a frame")
tf.app.flags.DEFINE_integer("width", 112, "width of a frame")
tf.app.flags.DEFINE_integer("decay_step", 1000, "learning rate decay step")
tf.app.flags.DEFINE_integer("iterations", 1000000, "number of iterations for training")
tf.app.flags.DEFINE_integer("start", 0, "number of iterations for training")
tf.app.flags.DEFINE_integer("stage", None, "stage index")

# frame config
tf.app.flags.DEFINE_integer("DELTA", 16, "frames per clip")
tf.app.flags.DEFINE_integer("IMG_SIZE", 112, "frames per clip")

# token config
tf.app.flags.DEFINE_integer("PAD", 0, "index of GO symbol")
tf.app.flags.DEFINE_integer("GO", 1, "index of GO symbol")
tf.app.flags.DEFINE_integer("EOS", 2, "index of EOS symbol")

# path
tf.app.flags.DEFINE_string("mean_file", 'train01_16_128_171_mean.npy', "path to mean file from sports1m dataset")
tf.app.flags.DEFINE_string("video_prefix", '/mnt/hosang/Dense_VTT/video', "prefix of video")
tf.app.flags.DEFINE_string("feats_home", None, "feats home dir")
tf.app.flags.DEFINE_string("GloVe_path", None, "path to pretrained GloVe vector file")
tf.app.flags.DEFINE_string("checkpoint_dir", '/mnt/hdd1/ckpt_repos/with_context_dnc_sports1m/step-last', "checkpoint directory")
tf.app.flags.DEFINE_string("logs_dir", './logs/sports1m', "logs directory")

# mode
tf.app.flags.DEFINE_string("mode", None, "support 'train_DNC/test_DNC/train_HRED/test_HRED' mode")
tf.app.flags.DEFINE_bool("use_gating", False, "apply gating for memory matrix")

# GPU selection
tf.app.flags.DEFINE_integer("gpu", 0, "gpu device index")

# for feature extraction
tf.app.flags.DEFINE_string("type", None, "train/val_1/val_2")
tf.app.flags.DEFINE_string("dir", None, "features target directory")
tf.app.flags.DEFINE_string("pretrained", 'sports1m', "pretrained model")

# for K-fold cross validation
tf.app.flags.DEFINE_integer("K", None, "index of K-fold cross validation")

FLAGS = tf.app.flags.FLAGS