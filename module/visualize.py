import os
import matplotlib.pyplot as plt
from config import FLAGS

def visualize_logs(train, valid):
    if os.path.exists(train) and os.path.exists(valid):
        with open(train) as f:
            train_lines = f.readlines()
        train_steps, train_losses = zip(*map(lambda x: x.strip().split('\t'), train_lines))

        train_steps = map(lambda x: eval(x), train_steps)
        train_losses = map(lambda x: eval(x), train_losses)

        with open(valid) as f:
            valid_lines = f.readlines()
        valid_steps, valid_losses = zip(*map(lambda x: x.strip().split('\t'), valid_lines))

        valid_steps = map(lambda x: eval(x), valid_steps)
        valid_losses = map(lambda x: eval(x), valid_losses)

        plt.plot(train_steps, train_losses, 'b', valid_steps, valid_losses, 'r')
        plt.show()

# DNC
visualize_logs(os.path.join(FLAGS.logs_dir,'DNC_train.txt'),
               os.path.join(FLAGS.logs_dir,'DNC_val.txt'))

# HRED
visualize_logs(os.path.join(FLAGS.logs_dir,'HRED_train.txt'),
               os.path.join(FLAGS.logs_dir,'HRED_val.txt'))
