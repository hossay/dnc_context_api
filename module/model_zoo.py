import tensorflow as tf
import numpy as np
import time
import sys, os
import json
import collections
import re
import i3d

from dnc.dnc import DNC
from dnc.recurrent_controller import RecurrentController
import tf_decoder
from config import FLAGS

# dict word->ix
word2ix = json.load(file('./module/word2ix.json'))

# tf flags configurations
checkpoint_dir = FLAGS.checkpoint_dir
iterations = FLAGS.iterations

batch_size = FLAGS.batch_size
input_size = FLAGS.input_size
output_size = FLAGS.output_size
word_space_size = len(word2ix)
words_count = FLAGS.words_count
word_size = FLAGS.word_size
read_heads = FLAGS.read_heads

learning_rate = FLAGS.learning_rate

hidden_size = FLAGS.hidden_size         # decoder hidden size
embedding_size = FLAGS.embedding_size

class I3DNet:
    def __init__(self, inps, pretrained_model_path, final_end_point, trainable=False, scope='v/SenseTime_I3D'):

        self.final_end_point = final_end_point
        self.trainable = trainable
        self.scope = scope

        # build entire pretrained networks (dummy operation!)
        i3d.I3D(inps, scope=scope, is_training=trainable)

        var_dict = { re.sub(r':\d*','',v.name):v for v in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='v/SenseTime_I3D') }
        self.assign_ops = []
        for var_name, var_shape in tf.contrib.framework.list_variables(pretrained_model_path):
            # load variable
            var = tf.contrib.framework.load_variable(pretrained_model_path, var_name)
            assign_op = var_dict[var_name].assign(var)
            self.assign_ops.append(assign_op)

    def __call__(self, inputs):
        feats, _ = i3d.I3D(inputs, final_endpoint=self.final_end_point, scope=self.scope, is_training=self.trainable)
        return feats

class C3DNet:
    def __init__(self, pretrained_model_path, scope=None, trainable=True):
        if scope == None:
            self.scope = 'C3D'

        # self.keep_rate = keep_rate

        with tf.variable_scope(self.scope):
            # load pre-trained weights(C3D)
            self._weights = {}
            self._biases = {}
            for var_name, var_shape in tf.contrib.framework.list_variables(pretrained_model_path):
                # load variable
                var = tf.contrib.framework.load_variable(pretrained_model_path, var_name)
                var_dict = self._biases if len(var_shape) == 1 else self._weights

                var_dict[var_name.split('/')[-1]] = tf.get_variable(var_name,
                                                                    var_shape,
                                                                    initializer=tf.constant_initializer(var),
                                                                    dtype='float32',
                                                                    trainable=trainable)

    def __call__(self, inputs):
        def conv3d(name, l_input, w, b):
            return tf.nn.bias_add(
                tf.nn.conv3d(l_input, w, strides=[1, 1, 1, 1, 1], padding='SAME'),
                b, name=name)

        def max_pool(name, l_input, k):
            return tf.nn.max_pool3d(l_input, ksize=[1, k, 2, 2, 1], strides=[1, k, 2, 2, 1], padding='SAME', name=name)

        # Convolution Layer
        conv1 = conv3d('conv1', inputs, self._weights['wc1'], self._biases['bc1'])
        conv1 = tf.nn.relu(conv1, 'relu1')
        pool1 = max_pool('pool1', conv1, k=1)

        # Convolution Layer
        conv2 = conv3d('conv2', pool1, self._weights['wc2'], self._biases['bc2'])
        conv2 = tf.nn.relu(conv2, 'relu2')
        pool2 = max_pool('pool2', conv2, k=2)

        # Convolution Layer
        conv3 = conv3d('conv3a', pool2, self._weights['wc3a'], self._biases['bc3a'])
        conv3 = tf.nn.relu(conv3, 'relu3a')
        conv3 = conv3d('conv3b', conv3, self._weights['wc3b'], self._biases['bc3b'])
        conv3 = tf.nn.relu(conv3, 'relu3b')
        pool3 = max_pool('pool3', conv3, k=2)

        # Convolution Layer
        conv4 = conv3d('conv4a', pool3, self._weights['wc4a'], self._biases['bc4a'])
        conv4 = tf.nn.relu(conv4, 'relu4a')
        conv4 = conv3d('conv4b', conv4, self._weights['wc4b'], self._biases['bc4b'])
        conv4 = tf.nn.relu(conv4, 'relu4b')
        pool4 = max_pool('pool4', conv4, k=2)

        # Convolution Layer
        conv5 = conv3d('conv5a', pool4, self._weights['wc5a'], self._biases['bc5a'])
        conv5 = tf.nn.relu(conv5, 'relu5a')
        conv5 = conv3d('conv5b', conv5, self._weights['wc5b'], self._biases['bc5b'])
        conv5 = tf.nn.relu(conv5, 'relu5b')
        pool5 = max_pool('pool5', conv5, k=2)

        # Fully connected layer
        # pool5 = tf.transpose(pool5, perm=[0, 1, 4, 2, 3]) # only for ucf
        dense1 = tf.reshape(pool5, [batch_size, self._weights['wd1'].get_shape().as_list()[
            0]])  # Reshape conv3 output to fit dense layer input
        dense1 = tf.matmul(dense1, self._weights['wd1']) + self._biases['bd1']

        dense1 = tf.nn.relu(dense1, name='fc1')  # Relu activation
        #dense1 = tf.nn.dropout(dense1, self.keep_rate)

        dense2 = tf.nn.relu(tf.matmul(dense1, self._weights['wd2']) + self._biases['bd2'], name='fc2')  # Relu activation
        #dense2 = tf.nn.dropout(dense2, self.keep_rate)

        # Output: class prediction
        # out = tf.matmul(dense2, self._weights['wout']) + self._biases['bout']

        return dense2





def llprint(message):
    sys.stdout.write(message)
    sys.stdout.flush()

def inv_dict(dictionary):
    return {v: k for k, v in dictionary.iteritems()}

def write_logs(fn, step, loss, score):
    fd = open(fn, 'a+')
    line = '{}\t{}\t{}\n'.format(step, loss, score)
    fd.write(line)
    fd.close()

def build_graph(ph, is_train):
    llprint("Building Computational Graph for DNC ...\n")

    with tf.variable_scope('DNC', reuse=tf.AUTO_REUSE):
        ncomputer = DNC(controller_class=RecurrentController,
                        input_size=input_size,
                        output_size=output_size,
                        keep_prob=ph.get('keep_prob', 1.0),
                        initial_memory_state=None,
                        memory_words_num=words_count,
                        memory_word_size=word_size,
                        memory_read_heads=read_heads,
                        batch_size=batch_size)

    with tf.variable_scope('embedding_layer', reuse=tf.AUTO_REUSE):
        embedding = tf.get_variable(name='embedding', shape=[word_space_size, embedding_size])

    # dec_cell
    dec_cell = tf.nn.rnn_cell.GRUCell(num_units=hidden_size)

    def loop_body(t, multi_dec_logits, memory_state):
        ncomputer.build_graph(input_data=tf.reverse(ph['multi_input_data'][t], axis=[1]), # reverse input data to reduce curse of padding
                              initial_memory_state=memory_state)
        outputs, memory_veiw = ncomputer.get_outputs()
        new_memory_state = memory_veiw['new_memory_state'] if FLAGS.with_context else ncomputer.memory.init_memory(None)

        initial_state = outputs[:, -1]

        if is_train:
            # build computation graph for decoder
            dec_logits = tf_decoder.teacher_decoder(cell=dec_cell,
                                                    inputs=ph['multi_dec_in'][t],
                                                    embedding=embedding,
                                                    initial_state=initial_state,
                                                    word_space_size=word_space_size,
                                                    sequence_length=None)
        else:
            dec_logits = tf_decoder.greedy_decoder(cell=dec_cell,
                                                   embedding=embedding,
                                                   initial_state=initial_state,
                                                   word_space_size=word_space_size,
                                                   dec_maxlen=FLAGS.dec_maxlen)
            # padded logits
            dec_logits = tf.pad(dec_logits,
                                [[0,0],[0,FLAGS.dec_maxlen-tf.shape(dec_logits)[1]],[0,0]])

        multi_dec_logits = multi_dec_logits.write(t, dec_logits)

        return t + 1, multi_dec_logits, new_memory_state

    memory_state = ncomputer.memory.init_memory(None)
    t = tf.constant(0)

    multi_dec_logits = tf.TensorArray(dtype=tf.float32, size=ph['n_events'])
    _, multi_dec_logits, _ = tf.while_loop(cond=lambda t,*_ : t<ph['n_events'],
                                           body=loop_body,
                                           loop_vars=(t, multi_dec_logits,memory_state))


    return dict(ncomputer=ncomputer,
                embedding=embedding,
                multi_dec_logits=multi_dec_logits.stack())